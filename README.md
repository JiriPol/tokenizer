# Tokenization

.NET Standard library and console application for tokenizing text.

## Library usage examples

```
Stream settingStream = File.OpenRead("settings.xml");   
ITokenizerSettingsReader tokenizerSettingsReader = new SimpleWordTokenizerSettingsReaderXml(settingStream);              
ITokenizerSettings tokenizerSettings = tokenizerSettingsReader.Read();  
ITokenizer simpleWordTokenizer = new SimpleWordTokenizer(tokenizerSettings);
IEnumerable<IToken> enumerable = simpleWordTokenizer.Tokenize("Text to tokenize.");
using (IEnumerator<IToken> enumerator = enumerable.GetEnumerator())
{
	while (enumerator.MoveNext())
    {
		Token token = enumerator.Current as Token;
		Console.WriteLine($"{token.Type.ToString()},{token.Text},{token.TokenRange.Start},{token.TokenRange.End}");
    }
}

```

Outputs:

StartOfStream,,0,0

Word,Text,0,4

Whitespace,,4,5

Word,to,5,7

Whitespace,,7,8

Word,tokenize,8,16

Punctuation,.,16,17

EndOfStream,,17,17

settings.xml contains:

```
<?xml version="1.0" encoding="utf-8"?>
<settings xmlns="http://vokabular.ujc.cas.cz/schemas/diachronic-nlp/setting">
    <setting type="SimpleWordTokenizer">
        <punctaionCharacters type="chars">
            <item>[</item>
            <item>]</item>
            <item>'</item>
            <item>:</item>
            <item>"</item>
            <item>“</item>
            <item>„</item>
            <item>;</item>
            <item>,</item>
            <item>.</item>
            <item>/</item>
            <item>?</item>
            <item>-</item>
            <item>–</item>
            <item>{</item>
            <item>}</item>
        </punctaionCharacters>
        <whiteSpaces type="chars" format="hex">
            <item hint="SPACE (U+0020)">0x0D</item>
            <item hint="LINE FEED (LF) (U+000A)">0x20</item>
            <item hint="CARRIAGE RETURN (CR) (U+000D)">0x0A</item>
        </whiteSpaces>
    </setting>
</settings>

```


## Console app usage examples

```
dotnet ConsoleTokenizer.dll -i "C:\Users\polacek\Documents\tokenizer\text.txt" -o "C:\Users\polacek\Documents\tokenizer\output.csv" -s "C:\Users\polacek\Documents\tokenizer\settings.xml"


```


## Supported Platforms

* .NET Standard 2.0


## Contributing

If you want to contribute, please feel free to grab an issue. To get your code into the repository please fork it and create a pull request with your changes.

## Authors

* **Jiri Polacek**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
