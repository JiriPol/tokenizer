﻿using System;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    /// <summary>
    /// <see cref="Token"/> is immutable implemetation of the <see cref="IToken"/> interface.
    /// </summary>
    public abstract class Token : IToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Token"/> class.
        /// </summary>
        /// <param name="tokenRange">Range of token.</param>
        /// <param name="text">Token text.</param>
        /// <param name="tokenType"></param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="tokenRange"/> or <paramref name="text"/> is null reference.</exception>
        protected Token(ITokenRange tokenRange, string text, TokenType tokenType)
        {
            if ((tokenRange == null) || (text == null))
            {
               throw new ArgumentNullException();
            }

            TokenRange = tokenRange;
            Text = text;
            Type = tokenType;
        }

        public ITokenRange TokenRange { get; }
        public string Text { get; }
        public TokenType Type { get; }
    }
}