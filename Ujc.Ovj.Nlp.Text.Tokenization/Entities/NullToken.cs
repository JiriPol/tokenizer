﻿using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    public class NullToken : Token
    {
        public NullToken(uint position) 
            : base(new TokenRange(position, position), string.Empty, TokenType.Null)
        {
        }
    }
}