﻿using System;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    /// <summary>
    /// <see cref="TokenRange"/> is immutable implemetation of the <see cref="ITokenRange"/> interface.
    /// </summary>
    public class TokenRange : ITokenRange
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenRange"/> class.
        /// </summary>
        /// <param name="start">Start position of token range.</param>
        /// <param name="end">End position of token range.</param>
        /// <exception cref="ArgumentException">Thrown if <paramref name="start"/> &gt; <paramref name="end"/>.</exception>
        public TokenRange(uint start, uint end)
        {
            if (start > end)
            {
                throw new ArgumentException();
            }

            Start = start;
            End = end;
        }

        /// <summary>
        /// Start of this range (read-only)
        /// </summary>
        public uint Start { get; }

        /// <summary>
        /// End of this range (read-only)
        /// </summary>
        public uint End { get; }

        /// <inheritdoc cref="object.Equals(object)"/>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            TokenRange tr = (TokenRange) obj;
            if (Start == tr.Start && End == tr.End)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc cref="object.GetHashCode"/>
        public override int GetHashCode()
        {
            uint xor = Start ^ End; //XOR is safe here, because (Start <= End).
            return unchecked((int)xor);
        }

        /// <inheritdoc cref="object.ToString"/>
        /// <returns>String representation in format &lt;<see cref="Start"/>;<see cref="End"/>&gt;</returns>
        public override string ToString()
        {
            return $"<{Start};{End}>";
        }
    }
}