﻿using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// NumberToken does not check if text is actual number.
    /// </remarks>
    public class NumberToken : Token
    {
        public NumberToken(ITokenRange tokenRange, string text) 
            : base(tokenRange, text, TokenType.Number)
        {
            //NOP.
        }
    }
}