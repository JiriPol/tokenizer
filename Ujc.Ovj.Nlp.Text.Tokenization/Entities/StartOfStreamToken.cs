﻿using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    /// <summary>
    /// Token with fixed range &lt;0;0&gt; empty text and TokenType=<see cref="TokenType.StartOfStream"/>
    /// </summary>
    public class StartOfStreamToken : Token
    {
        public StartOfStreamToken() 
            : base(new TokenRange(0, 0), string.Empty, TokenType.StartOfStream)
        {
            //NOP.
        }
    }
}