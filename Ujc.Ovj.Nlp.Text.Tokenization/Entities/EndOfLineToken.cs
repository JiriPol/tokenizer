﻿using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    public class EndOfLineToken : Token
    {
        public EndOfLineToken(ITokenRange tokenRange, string text) 
            : base(tokenRange, text, TokenType.EndOfLine)
        {
            //NOP.
        }
    }
}