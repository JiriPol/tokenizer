﻿using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    public class EndOfStreamToken : Token
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="EndOfStreamToken"/> class.
        /// </summary>
        public EndOfStreamToken(uint position) 
            : base(new TokenRange(position, position), string.Empty, TokenType.EndOfStream)
        {
            //NOP.
        }

    }
}