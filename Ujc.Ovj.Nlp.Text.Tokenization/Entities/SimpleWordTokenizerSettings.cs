﻿using System;
using System.Collections.Generic;
using Ujc.Ovj.Nlp.Text.Tokenization.Interfaces;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    public class SimpleWordTokenizerSettings : ITokenizerSettings
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleWordTokenizerSettings"/> class.
        /// </summary>
        /// <param name="whitespaceList"></param>
        /// <param name="punctuationList"></param>
        /// <param name="newLineList"></param>
        /// <param name="abbrevitationList"></param>
        /// <param name="wordPartList"></param>
        /// <exception cref="ArgumentNullException">Thrown if any of arguments is null reference.</exception>
        public SimpleWordTokenizerSettings(IReadOnlyCollection<char> whitespaceList,
            IReadOnlyCollection<char> punctuationList,
            IReadOnlyCollection<char> newLineList,
            IReadOnlyCollection<string> abbrevitationList,
            IReadOnlyCollection<char> wordPartList)
        {
            if ((whitespaceList == null) || (punctuationList == null) || (newLineList == null) || (abbrevitationList == null) || (wordPartList == null))
            {
                throw new ArgumentNullException();
            }

            WhitespaceList = whitespaceList;
            PunctuationList = punctuationList;
            NewLineList = newLineList;
            AbbrevitationList = abbrevitationList;
            WordPartList = wordPartList;
        }

        public IReadOnlyCollection<char> WhitespaceList { get; }
        public IReadOnlyCollection<char> PunctuationList { get; }
        public IReadOnlyCollection<char> NewLineList { get; }
        public IReadOnlyCollection<string> AbbrevitationList { get; }
        public IReadOnlyCollection<char> WordPartList { get; }
    }
}