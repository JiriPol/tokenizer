﻿using System;
using System.Collections.Generic;
using Ujc.Ovj.Nlp.Text.Tokenization.Interfaces;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    public class NullTokenizerSettings : ITokenizerSettings
    {
        public IReadOnlyCollection<char> WhitespaceList => Array.Empty<char>();
        public IReadOnlyCollection<char> PunctuationList => Array.Empty<char>();
        public IReadOnlyCollection<char> NewLineList => Array.Empty<char>();
        public IReadOnlyCollection<string> AbbrevitationList => Array.Empty<string>();
        public IReadOnlyCollection<char> WordPartList => Array.Empty<char>();
    }
}