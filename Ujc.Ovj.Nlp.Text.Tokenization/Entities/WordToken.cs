﻿using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Entities
{
    public class WordToken : Token
    {
        public WordToken(ITokenRange tokenRange, string text) 
            : base(tokenRange, text, TokenType.Word)
        {
            //NOP.
        }
    }
}