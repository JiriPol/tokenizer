﻿using System.Collections.Generic;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Interfaces
{
    public interface ITokenizerSettings
    {
        IReadOnlyCollection<char> WhitespaceList { get; }
        IReadOnlyCollection<char> PunctuationList { get; }
        IReadOnlyCollection<char> NewLineList { get; }
        IReadOnlyCollection<string> AbbrevitationList { get; }
        IReadOnlyCollection<char> WordPartList { get; }
    }
}