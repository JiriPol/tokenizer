﻿namespace Ujc.Ovj.Nlp.Text.Tokenization.Interfaces
{
    public interface ITokenizerSettingsReader
    {
        ITokenizerSettings Read();
    }
}