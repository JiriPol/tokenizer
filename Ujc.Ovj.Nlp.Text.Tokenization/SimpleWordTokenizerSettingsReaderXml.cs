﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using NLog;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Interfaces;

namespace Ujc.Ovj.Nlp.Text.Tokenization
{
    public class SimpleWordTokenizerSettingsReaderXml : ITokenizerSettingsReader
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly XNamespace m_namespace = "http://vokabular.ujc.cas.cz/schemas/diachronic-nlp/setting";
        private readonly Stream m_stream;

        private const string FORMAT_ATTRIBUTE_NAME = "format";
        private const string FORMAT_ATTRIBUTE_HEX_VALUE = "hex";

        private const string PUNCTATION_CHARS = "punctaionCharacters";
        private const string ABBREVITATIONS = "abbreviations";
        private const string WHITESPACES = "whiteSpaces";
        private const string NEWLINE_CHARACTERS = "newlineCharacters";
        private const string PARTS_OF_THE_WORD = "partsOfTheWord";

        /// <summary>
        /// Contructor.
        /// </summary>
        /// <param name="stream">Stream with xml settings.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="stream"/> is null reference.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="stream"/> is not readable.</exception>
        public SimpleWordTokenizerSettingsReaderXml(Stream stream)
        {
            Logger.Debug($"Creating {nameof(SimpleWordTokenizerSettingsReaderXml)}.");
            ValidateStream(stream);

            m_stream = stream;
        }

        public ITokenizerSettings Read()
        {
            XDocument settingsXml = XDocument.Load(m_stream);
            XElement setting = settingsXml
                .Descendants(m_namespace + "setting")
                .FirstOrDefault(e => e.Attribute("type") != null && e.Attribute("type")?.Value == "SimpleWordTokenizer");

            if (setting == null)
            {
                return new NullTokenizerSettings();
            }

            List<char> whiteSpaceList = new List<char>();
            List<char> punctuationList = new List<char>();
            List<char> newLineList = new List<char>();
            List<string> abbrevitationList = new List<string>();
            List<char> wordPartList = new List<char>();

            IEnumerable<XElement> settingElements = setting.Elements();

            XName punctaionChars = m_namespace + PUNCTATION_CHARS;
            XName abbrevitations = m_namespace + ABBREVITATIONS;
            XName whiteSpaces = m_namespace + WHITESPACES;
            XName newlineCharacters = m_namespace + NEWLINE_CHARACTERS;
            XName partsOfTheWord = m_namespace + PARTS_OF_THE_WORD;

            foreach (XElement element in settingElements)
            {
                if (element.Name == punctaionChars)
                {
                    ReadCharacters(element, punctuationList);
                }
                if (element.Name == whiteSpaces)
                {
                    ReadCharacters(element, whiteSpaceList);
                }
                if (element.Name == newlineCharacters)
                {
                    ReadCharacters(element, newLineList);
                }
                if (element.Name == partsOfTheWord)
                {
                    ReadCharacters(element, wordPartList);
                }
                if (element.Name == abbrevitations)
                {
                    IEnumerable<XElement> items = element.Elements(m_namespace + "item");
                    abbrevitationList.AddRange(from item in items select item.Value);
                }
            }

            return new SimpleWordTokenizerSettings(whiteSpaceList, punctuationList, newLineList, abbrevitationList, wordPartList);
        }

        private static void ValidateStream(Stream stream)
        {
            Logger.Debug($"Validating {nameof(stream)}.");
            if (stream == null)
            {
                Logger.Error($"Argument {nameof(stream)} is null reference.");
                throw new ArgumentNullException();
            }

            if (!stream.CanRead)
            {
                Logger.Error($"Argument {nameof(stream)} is not readable.");
                throw new ArgumentException();
            }

        }

        private static char ParseUtf8HexCharacter(string utf8Hex)
        {
            string utf8HexWithoutPrefix = utf8Hex.StartsWith("0x", StringComparison.CurrentCultureIgnoreCase) ||
                                          utf8Hex.StartsWith("&H", StringComparison.CurrentCultureIgnoreCase)
                ? utf8Hex.Substring(2)
                : utf8Hex;


            return (char)Int16.Parse(utf8HexWithoutPrefix, NumberStyles.HexNumber);
        }

        private void ReadCharacters(XElement element, List<char> charList)
        {
            IEnumerable<XElement> items = element.Elements(m_namespace + "item");
            string[] a = items.Select(i => i.Value).ToArray();

            XAttribute formatAttribute = element.Attribute(FORMAT_ATTRIBUTE_NAME);
            if (formatAttribute != null &&
                formatAttribute.Value.Equals(FORMAT_ATTRIBUTE_HEX_VALUE, StringComparison.CurrentCultureIgnoreCase))
            {
                charList.AddRange(a.Select(ParseUtf8HexCharacter));
            }
            else
            {
                charList.AddRange(a.Select(s => s[0]));
            }
        }
    }
}