﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Interfaces;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization
{
    public class SimpleWordTokenizer : ITokenizer
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger(); 
        private readonly ITokenizerSettings m_tokenizerSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleWordTokenizer"/> class.
        /// </summary>
        /// <param name="tokenizerSettings">Tokenizer settings.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="tokenizerSettings"/> is null reference.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="tokenizerSettings"/> is not valid.</exception>
        public SimpleWordTokenizer(ITokenizerSettings tokenizerSettings)
        {
            Logger.Debug("Creating SimpleWordTokenizer");
            ValidateTokenizerSettings(tokenizerSettings);

            m_tokenizerSettings = tokenizerSettings;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">Text to tokenize.</param>
        /// <returns>Enumerator of tokens.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="text"/> is null reference.</exception>
        public IEnumerable<IToken> Tokenize(string text)
        {
            Logger.Debug("Tokenizing text");
            if (text == null)
            {
                Logger.Error("Tokenized text is null reference.");
                throw new ArgumentNullException();
            }

            Token streamStartToken = new StartOfStreamToken();
            yield return streamStartToken;

            int capacity = text.Length;
            StringBuilder stringBuilder = new StringBuilder(capacity);         

            uint position = 0;

            foreach (char c in text)
            {              
                if (IsWhitespace(c))
                {
                    //Char c is whitespace.
                    Token token = ProcessCurrentToken(ref stringBuilder, position, capacity);

                    if (token.Type != TokenType.Null)
                    {
                        yield return token;

                        position += token.TokenRange.End - token.TokenRange.Start;
                    }

                    token = new WhitespaceToken(new TokenRange(position, position + 1), c.ToString());
                    yield return token;

                    position += token.TokenRange.End - token.TokenRange.Start;
                }
                else if (IsPunctuation(c))
                {
                    //Char c is punctuation.
                    Token token = ProcessCurrentToken(ref stringBuilder, position, capacity);

                    if (token.Type != TokenType.Null)
                    {
                        yield return token;

                        position += token.TokenRange.End - token.TokenRange.Start;
                    }

                    token = new PunctuationToken(new TokenRange(position, position + 1), c.ToString());
                    yield return token;

                    position += token.TokenRange.End - token.TokenRange.Start;
                }
                else if (IsNewline(c))
                {
                    //Char c is newline
                    if (stringBuilder.Length > 0)
                    {
                        if (IsNewline(stringBuilder[stringBuilder.Length - 1]))
                        {
                            stringBuilder.Append(c);
                        }
                        else
                        {
                            Token token = ProcessCurrentToken(ref stringBuilder, position, capacity);
                            if (token.Type != TokenType.Null)
                            {
                                yield return token;
                            }

                            stringBuilder.Append(c);
                        }
                    }
                    else
                    {
                        stringBuilder.Append(c);
                    }
                }
                else
                {
                    if (ContainsNewLine(stringBuilder))
                    {
                        Token token = ProcessCurrentToken(ref stringBuilder, position, capacity);
                        if (token.Type != TokenType.Null)
                        {
                            yield return token;

                            position += token.TokenRange.End - token.TokenRange.Start;
                        }
                    }

                    stringBuilder.Append(c);
                }
            }

            Token tokenL = ProcessCurrentToken(ref stringBuilder, position, capacity);
            if (tokenL.Type != TokenType.Null)
            {
                yield return tokenL;

                position += tokenL.TokenRange.End - tokenL.TokenRange.Start;
            }

            if (position == uint.MaxValue)
            {
                position++;
            }

            IToken endOfStreamToken = new EndOfStreamToken((uint)position);
            yield return endOfStreamToken;
        }

        private bool IsWhitespace(char c)
        {
            return m_tokenizerSettings?.WhitespaceList?.Contains(c) ?? false;
        }

        private bool IsPunctuation(char c)
        {
            return m_tokenizerSettings?.PunctuationList?.Contains(c) ?? false;
        }

        private bool IsNewline(char c)
        {
            return m_tokenizerSettings?.NewLineList?.Contains(c) ?? false;
        }

        private Token ProcessCurrentToken(ref StringBuilder stringBuilder, uint position, int capacity)
        {  
            if (stringBuilder.Length == 0)
            {
                return new NullToken(position);
            }

            Token token;
            if (ContainsNumbersOnly(stringBuilder))
            {
                token = new NumberToken(new TokenRange(position, position + (uint)stringBuilder.Length), stringBuilder.ToString());
                stringBuilder = new StringBuilder(capacity);
            }
            else if (ContainsNewLine(stringBuilder))
            {
                token = new EndOfLineToken(new TokenRange(position, position + (uint)stringBuilder.Length), stringBuilder.ToString());
                stringBuilder = new StringBuilder(capacity);
            }
            else
            {
                token = new WordToken(new TokenRange(position, position + (uint)stringBuilder.Length), stringBuilder.ToString());
                stringBuilder = new StringBuilder(capacity);
            }

            return token;
        }

        private static bool ContainsNumbersOnly(StringBuilder stringBuilder)
        {
            for (int i = 0; i < stringBuilder.Length; i++)
            {
                if (!char.IsDigit(stringBuilder[i]))
                {
                    return false;
                }                   
            }

            return true;
        }

        private static bool ContainsNewLine(StringBuilder stringBuilder)
        {
            if (stringBuilder == null || stringBuilder.Length == 0)
            {
                return false;
            }

            if (stringBuilder.Length == 1 && (stringBuilder[0] == '\r' || stringBuilder[0] == '\n'))
            {
                return true;
            }

            if (stringBuilder.Length == 2 && ((stringBuilder[0] == '\r' && stringBuilder[1] == '\n') ||
                                              (stringBuilder[0] == '\n' && stringBuilder[1] == '\r')))
            {
                return true;
            }

            return false;
        }

        private static void ValidateTokenizerSettings(ITokenizerSettings tokenizerSettings)
        {
            Logger.Debug($"Validating {nameof(tokenizerSettings)}.");
            if (tokenizerSettings == null)
            {
                Logger.Error($"{nameof(tokenizerSettings)} is null reference.");
                throw new ArgumentNullException();
            }

            bool shouldThrowException = false;

            if (tokenizerSettings.PunctuationList == null)
            {
                Logger.Error($"{nameof(tokenizerSettings.PunctuationList)} in {nameof(tokenizerSettings)} is null reference.");
                shouldThrowException = true;
            }

            if (tokenizerSettings.AbbrevitationList == null)
            {
                Logger.Error($"{nameof(tokenizerSettings.AbbrevitationList)} in {nameof(tokenizerSettings)} is null reference.");
                shouldThrowException = true;
            }

            if (tokenizerSettings.NewLineList == null)
            {
                Logger.Error($"{nameof(tokenizerSettings.NewLineList)} in {nameof(tokenizerSettings)} is null reference.");
                shouldThrowException = true;
            }

            if (tokenizerSettings.WhitespaceList == null)
            {
                Logger.Error($"{nameof(tokenizerSettings.WhitespaceList)} in {nameof(tokenizerSettings)} is null reference.");
                shouldThrowException = true;
            }

            if (tokenizerSettings.WordPartList == null)
            {
                Logger.Error($"{nameof(tokenizerSettings.WordPartList)} in {nameof(tokenizerSettings)} is null reference.");
                shouldThrowException = true;
            }

            if (shouldThrowException)
            {
                throw new ArgumentException($"{nameof(tokenizerSettings)} is not valid. See log for additional info.");
            }
        }
    }
}