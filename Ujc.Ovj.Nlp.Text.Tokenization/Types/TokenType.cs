﻿namespace Ujc.Ovj.Nlp.Text.Tokenization.Types
{
    /// <summary>
    /// Enumeration of token types.
    /// </summary>
    public enum TokenType
    {
        StartOfStream,
        Word,
        Number,
        Whitespace,
        Punctuation,
        EndOfLine,
        EndOfStream,
        Null
    }
}