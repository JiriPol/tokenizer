﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using McMaster.Extensions.CommandLineUtils;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Interfaces;

namespace Ujc.Ovj.Nlp.Text.Tokenization.ConsoleApp
{
    //USE IT LIKE
    //dotnet ConsoleTokenizer.dll -i "C:\Users\polacek\Documents\tokenizer\text.txt" -o "C:\Users\polacek\Documents\tokenizer\output.csv" -s "C:\Users\polacek\Documents\tokenizer\settings.xml"
    class Program
    {
        static void Main(string[] args)
        {
            CommandLineApplication application = new CommandLineApplication();
            application.Name = "ConsoleTokenizer";
            application.Description = "Text tokenizer reads input text file encoded with UTF-8 and outputs csv file with tokens.";
            application.HelpOption("-?|-h|--help");

            CommandOption inputOption = application.Option("-i|--input\"<inputFilePath>\"", "Input text file path to tokenize.", CommandOptionType.SingleValue);
            CommandOption outputOption = application.Option("-o|--output\"<outputFilePath>\"", "Output CSV file path.", CommandOptionType.SingleValue);
            CommandOption settingsOption = application.Option("-s|--settings\"<settingsFilePath>\"", "Settings file path.", CommandOptionType.SingleValue);

            CommandOption verboseOption = application.Option("-v|--verbose", "Enable verbose console output", CommandOptionType.NoValue);        
            CommandOption noninteraciveOption = application.Option("-n|--noninteractive", "Run non-interactively", CommandOptionType.NoValue);

            application.OnExecute(() =>
            {
                if (verboseOption.HasValue())
                {
                    application.Out.WriteLine("Executing");
                }

                if (inputOption.HasValue() && outputOption.HasValue())
                {
                    Stream settingStream;
                    try
                    {
                        if (verboseOption.HasValue())
                        {
                            application.Out.WriteLine("Opening settings file.");
                        }
                        settingStream = File.OpenRead(settingsOption.Value());                       
                    }
                    catch (Exception e)
                    {
                        application.Error.WriteLine(e.Message);
                        return -1;
                    }

                    ITokenizerSettings tokenizerSettings;
                    if (settingsOption.HasValue())
                    {
                        if (verboseOption.HasValue())
                        {
                            application.Out.WriteLine("Reading settings file.");
                        }
                        ITokenizerSettingsReader tokenizerSettingsReader = new SimpleWordTokenizerSettingsReaderXml(settingStream);
                        tokenizerSettings = tokenizerSettingsReader.Read();                       
                    }
                    else
                    {
                        if (verboseOption.HasValue())
                        {
                            application.Out.WriteLine("Using default settings.");
                        }
                        tokenizerSettings = new NullTokenizerSettings();
                    }

                    
                    ITokenizer simpleWordTokenizer = new SimpleWordTokenizer(tokenizerSettings);
                    string sourceText;
                    try
                    {
                        if (verboseOption.HasValue())
                        {
                            application.Out.WriteLine("Reading source text.");
                        }
                        sourceText = File.ReadAllText(inputOption.Value());
                    }
                    catch (Exception e)
                    {
                        application.Error.WriteLine(e.Message);
                        return -1;
                    }

                    if (verboseOption.HasValue())
                    {
                        application.Out.WriteLine("Tokenizing.");
                    }
                    IEnumerable<IToken> enumerable = simpleWordTokenizer.Tokenize(sourceText);
                    FileStream fileStream;
                    bool outputFileExists = File.Exists(outputOption.Value());
                    if (outputFileExists && !noninteraciveOption.HasValue())
                    {
                        application.Out.WriteLine($"Output file \"{outputOption.Value()}\" already exists.");
                        bool yesNo = Prompt.GetYesNo("Overwrite file?", true);
                        if (!yesNo)
                        {
                            return 0;
                        }
                    }

                    try
                    {
                        if (verboseOption.HasValue())
                        {
                            application.Out.WriteLine("Opening output file for writing.");
                        }
                        fileStream = File.OpenWrite(outputOption.Value());
                    }
                    catch (Exception e)
                    {
                        application.Error.WriteLine(e.Message);
                        return -1;
                    }
                    
                    using (StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                    using (IEnumerator<IToken> enumerator = enumerable.GetEnumerator())
                    {
                        if (verboseOption.HasValue())
                        {
                            application.Out.WriteLine("Writing result.");
                        }
                        streamWriter.WriteLine("Type,Token,RangeStart,RangeEnd");

                        while (enumerator.MoveNext())
                        {
                            Token token = enumerator.Current as Token;
                            if (token == null)
                            {
                                application.Error.WriteLine("Error occured.");
                                return -1;
                            }

                            streamWriter.WriteLine($"\"{token.Type.ToString()}\",\"{token.Text.Replace("\"", "\"\"")}\",{token.TokenRange.Start},{token.TokenRange.End}");
                        }

                        streamWriter.Close();
                    }
                }
                else
                {
                    application.ShowHint();
                }

                if (verboseOption.HasValue())
                {
                    application.Out.WriteLine("Closing.");
                }

                return 0;
            });

            application.Execute(args);
        }
    }
}
