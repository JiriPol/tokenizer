﻿namespace Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities
{
    public interface IToken
    {
        /// <summary>
        /// Range of this token.
        /// </summary>
        ITokenRange TokenRange { get; }

        /// <summary>
        /// Text of this token.
        /// </summary>
        string Text { get; }
    }
}