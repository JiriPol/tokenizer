﻿namespace Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities
{
    public interface ITokenRange
    {
        /// <summary>
        /// Start position of this token.
        /// </summary>
        uint Start { get; }

        /// <summary>
        /// End position of this token.
        /// </summary>
        uint End { get; }
    }
}