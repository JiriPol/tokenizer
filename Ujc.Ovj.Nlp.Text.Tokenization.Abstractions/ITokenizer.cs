﻿using System.Collections.Generic;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Abstractions
{
    public interface ITokenizer
    {
        IEnumerable<IToken> Tokenize(string text);
    }
}