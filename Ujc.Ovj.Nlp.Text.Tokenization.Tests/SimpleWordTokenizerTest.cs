﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Abstractions.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Interfaces;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests
{
    [TestClass]
    public class SimpleWordTokenizerTest
    {
        [TestMethod]
        public void Constructor()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorNullSettings()
        {
            SimpleWordTokenizer swt = new SimpleWordTokenizer(null);
        }

        [TestMethod]
        public void TokenizeEmptyText()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();
            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);
            IEnumerable<IToken> enumerable = swt.Tokenize("");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(2 , tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("", tokenList[1].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[1].TokenRange.End);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TokenizeNullText()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize(null);
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();
        }

        [TestMethod]
        public void TokenizeSingleChar()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("a");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("a", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)1, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }


        [TestMethod]
        public void TokenizeSingleNumber()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("0");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("0", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)1, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Number, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeWhitespace()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new []{ ' ' }, new List<char>(), new List<char>(), new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize(" ");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();
            

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual(" ", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)1, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizePunctuation()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new List<char>(), new[] { '.' }, new List<char>(), new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize(".");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();
            

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual(".", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)1, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Punctuation, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeNewLineLf()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new List<char>(), new List<char>(), new[] { '\r', '\n' }, new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("\n");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("\n", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)1, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.EndOfLine, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeNewLineCr()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new List<char>(), new List<char>(), new[] { '\r', '\n' }, new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("\r");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();
            

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("\r", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)1, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)1, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.EndOfLine, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeNewLineLfCr()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new List<char>(), new List<char>(), new[] { '\r', '\n' }, new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("\n\r");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("\n\r", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)2, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)2, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)2, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.EndOfLine, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeNewLineCrLf()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new List<char>(), new List<char>(), new[] { '\r', '\n' }, new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("\r\n");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }
            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("\r\n", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)2, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)2, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)2, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.EndOfLine, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeWord()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("slovo");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("slovo", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeWordWithNumber()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("sl0vo");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("sl0vo", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeWordWithStartNumber()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("5lovo");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("5lovo", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeWordWithEndNumber()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("slov0");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();


            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("slov0", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeWordWithOnlyNumbers()
        {
            ITokenizerSettings nullTokenizerSettings = new NullTokenizerSettings();

            SimpleWordTokenizer swt = new SimpleWordTokenizer(nullTokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("51040");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(3, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("51040", tokenList[1].Text);
            Assert.AreEqual("", tokenList[2].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[2].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Number, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[2]).Type);
        }

        [TestMethod]
        public void TokenizeTwoWords()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new[] { ' ' }, new List<char>(), new List<char>(), new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("slovo jablko");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(5, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("slovo", tokenList[1].Text);
            Assert.AreEqual(" ", tokenList[2].Text);
            Assert.AreEqual("jablko", tokenList[3].Text);
            Assert.AreEqual("", tokenList[4].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)6, tokenList[2].TokenRange.End);

            Assert.AreEqual((uint)6, tokenList[3].TokenRange.Start);
            Assert.AreEqual((uint)12, tokenList[3].TokenRange.End);

            Assert.AreEqual((uint)12, tokenList[4].TokenRange.Start);
            Assert.AreEqual((uint)12, tokenList[4].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[2]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[3]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[4]).Type);
        }

        [TestMethod]
        public void TokenizeSimpleSentence()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new[] { ' ' }, new []{'.'}, new List<char>(), new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("Venku je krásně.");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(8, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("Venku", tokenList[1].Text);
            Assert.AreEqual(" ", tokenList[2].Text);
            Assert.AreEqual("je", tokenList[3].Text);
            Assert.AreEqual(" ", tokenList[4].Text);
            Assert.AreEqual("krásně", tokenList[5].Text);
            Assert.AreEqual(".", tokenList[6].Text);
            Assert.AreEqual("", tokenList[7].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)6, tokenList[2].TokenRange.End);

            Assert.AreEqual((uint)6, tokenList[3].TokenRange.Start);
            Assert.AreEqual((uint)8, tokenList[3].TokenRange.End);

            Assert.AreEqual((uint)8, tokenList[4].TokenRange.Start);
            Assert.AreEqual((uint)9, tokenList[4].TokenRange.End);

            Assert.AreEqual((uint)9, tokenList[5].TokenRange.Start);
            Assert.AreEqual((uint)15, tokenList[5].TokenRange.End);

            Assert.AreEqual((uint)15, tokenList[6].TokenRange.Start);
            Assert.AreEqual((uint)16, tokenList[6].TokenRange.End);

            Assert.AreEqual((uint)16, tokenList[7].TokenRange.Start);
            Assert.AreEqual((uint)16, tokenList[7].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[2]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[3]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[4]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[5]).Type);
            Assert.AreEqual(TokenType.Punctuation, ((Token)tokenList[6]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[7]).Type);
        }

        [TestMethod]
        public void TokenizeMultilineLfSimpleSentence()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new[] { ' ' }, new[] { '.' }, new []{'\n'}, new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("Venku je \nkrásně.");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(9, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("Venku", tokenList[1].Text);
            Assert.AreEqual(" ", tokenList[2].Text);
            Assert.AreEqual("je", tokenList[3].Text);
            Assert.AreEqual(" ", tokenList[4].Text);
            Assert.AreEqual("\n", tokenList[5].Text);
            Assert.AreEqual("krásně", tokenList[6].Text);
            Assert.AreEqual(".", tokenList[7].Text);
            Assert.AreEqual("", tokenList[8].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)6, tokenList[2].TokenRange.End);

            Assert.AreEqual((uint)6, tokenList[3].TokenRange.Start);
            Assert.AreEqual((uint)8, tokenList[3].TokenRange.End);

            Assert.AreEqual((uint)8, tokenList[4].TokenRange.Start);
            Assert.AreEqual((uint)9, tokenList[4].TokenRange.End);

            Assert.AreEqual((uint)9, tokenList[5].TokenRange.Start);
            Assert.AreEqual((uint)10, tokenList[5].TokenRange.End);

            Assert.AreEqual((uint)10, tokenList[6].TokenRange.Start);
            Assert.AreEqual((uint)16, tokenList[6].TokenRange.End);

            Assert.AreEqual((uint)16, tokenList[7].TokenRange.Start);
            Assert.AreEqual((uint)17, tokenList[7].TokenRange.End);

            Assert.AreEqual((uint)17, tokenList[8].TokenRange.Start);
            Assert.AreEqual((uint)17, tokenList[8].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[2]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[3]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[4]).Type);
            Assert.AreEqual(TokenType.EndOfLine, ((Token)tokenList[5]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[6]).Type);
            Assert.AreEqual(TokenType.Punctuation, ((Token)tokenList[7]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[8]).Type);
        }

        [TestMethod]
        public void TokenizeMultilineCrLfSimpleSentence()
        {
            ITokenizerSettings tokenizerSettings = new SimpleWordTokenizerSettings(
                new[] { ' ' }, new[] { '.' }, new[] { '\n', '\r' }, new List<string>(), new List<char>());

            SimpleWordTokenizer swt = new SimpleWordTokenizer(tokenizerSettings);

            IEnumerable<IToken> enumerable = swt.Tokenize("Venku je \r\nkrásně.");
            IEnumerator<IToken> enumerator = enumerable.GetEnumerator();

            IList<IToken> tokenList = new List<IToken>();
            while (enumerator.MoveNext())
            {
                tokenList.Add(enumerator.Current);
            }

            enumerator.Dispose();

            Assert.AreEqual(9, tokenList.Count);
            Assert.AreEqual("", tokenList[0].Text);
            Assert.AreEqual("Venku", tokenList[1].Text);
            Assert.AreEqual(" ", tokenList[2].Text);
            Assert.AreEqual("je", tokenList[3].Text);
            Assert.AreEqual(" ", tokenList[4].Text);
            Assert.AreEqual("\r\n", tokenList[5].Text);
            Assert.AreEqual("krásně", tokenList[6].Text);
            Assert.AreEqual(".", tokenList[7].Text);
            Assert.AreEqual("", tokenList[8].Text);

            Assert.AreEqual((uint)0, tokenList[0].TokenRange.Start);
            Assert.AreEqual((uint)0, tokenList[0].TokenRange.End);

            Assert.AreEqual((uint)0, tokenList[1].TokenRange.Start);
            Assert.AreEqual((uint)5, tokenList[1].TokenRange.End);

            Assert.AreEqual((uint)5, tokenList[2].TokenRange.Start);
            Assert.AreEqual((uint)6, tokenList[2].TokenRange.End);

            Assert.AreEqual((uint)6, tokenList[3].TokenRange.Start);
            Assert.AreEqual((uint)8, tokenList[3].TokenRange.End);

            Assert.AreEqual((uint)8, tokenList[4].TokenRange.Start);
            Assert.AreEqual((uint)9, tokenList[4].TokenRange.End);

            Assert.AreEqual((uint)9, tokenList[5].TokenRange.Start);
            Assert.AreEqual((uint)11, tokenList[5].TokenRange.End);

            Assert.AreEqual((uint)11, tokenList[6].TokenRange.Start);
            Assert.AreEqual((uint)17, tokenList[6].TokenRange.End);

            Assert.AreEqual((uint)17, tokenList[7].TokenRange.Start);
            Assert.AreEqual((uint)18, tokenList[7].TokenRange.End);

            Assert.AreEqual((uint)18, tokenList[8].TokenRange.Start);
            Assert.AreEqual((uint)18, tokenList[8].TokenRange.End);

            Assert.AreEqual(TokenType.StartOfStream, ((Token)tokenList[0]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[1]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[2]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[3]).Type);
            Assert.AreEqual(TokenType.Whitespace, ((Token)tokenList[4]).Type);
            Assert.AreEqual(TokenType.EndOfLine, ((Token)tokenList[5]).Type);
            Assert.AreEqual(TokenType.Word, ((Token)tokenList[6]).Type);
            Assert.AreEqual(TokenType.Punctuation, ((Token)tokenList[7]).Type);
            Assert.AreEqual(TokenType.EndOfStream, ((Token)tokenList[8]).Type);
        }
    }
}
