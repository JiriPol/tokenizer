﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class PunctiationTokenTest
    {
        [TestMethod]
        public void ValuesTest()
        {
            PunctuationToken token = new PunctuationToken(new TokenRange(0,0), ".");

            Assert.IsNotNull(token);
            Assert.AreEqual(TokenType.Punctuation, token.Type);
            Assert.AreEqual(".", token.Text);
            Assert.AreEqual(new TokenRange(0, 0), token.TokenRange);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RangeNullTest()
        {
            PunctuationToken token = new PunctuationToken(null, "\r");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TextNullTest()
        {
            PunctuationToken token = new PunctuationToken(new TokenRange(0, 0), null);
        }
    }
}
