﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class NumberTokenTest
    {
        [TestMethod]
        public void ValuesTest()
        {
            NumberToken token = new NumberToken(new TokenRange(0,0), "5");

            Assert.IsNotNull(token);
            Assert.AreEqual(TokenType.Number, token.Type);
            Assert.AreEqual("5", token.Text);
            Assert.AreEqual(new TokenRange(0,0), token.TokenRange);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RangeNullTest()
        {
            NumberToken token = new NumberToken(null, "\r");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TextNullTest()
        {
            NumberToken token = new NumberToken(new TokenRange(0, 0), null);
        }
    }
}
