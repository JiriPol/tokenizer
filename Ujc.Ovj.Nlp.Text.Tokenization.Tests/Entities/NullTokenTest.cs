﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class NullTokenTest
    {
        [TestMethod]
        public void ValuesTest()
        {
            NullToken tokenTest = new NullToken(0);

            Assert.AreEqual(TokenType.Null, tokenTest.Type);
            Assert.AreEqual("", tokenTest.Text);
            Assert.AreEqual(new TokenRange(0,0), tokenTest.TokenRange);
        }
    }
}
