﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class WordTokenTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            WordToken token = new WordToken(new TokenRange(0,0), "slovo");

            Assert.IsNotNull(token);
            Assert.AreEqual("slovo", token.Text);
            Assert.AreEqual(TokenType.Word, token.Type);
            Assert.AreEqual(new TokenRange(0,0), token.TokenRange);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RangeNullTest()
        {
            WordToken token = new WordToken(null, "\r");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TextNullTest()
        {
            WordToken token = new WordToken(new TokenRange(0, 0), null);
        }
    }
}
