﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class StartOfStreamTokenTest
    {
        [TestMethod]
        public void ValuesTest()
        {
            StartOfStreamToken token = new StartOfStreamToken();

            Assert.IsNotNull(token);
            Assert.AreEqual(TokenType.StartOfStream, token.Type);
            Assert.AreEqual("", token.Text);
            Assert.AreEqual(new TokenRange(0, 0), token.TokenRange);
        }
    }
}
