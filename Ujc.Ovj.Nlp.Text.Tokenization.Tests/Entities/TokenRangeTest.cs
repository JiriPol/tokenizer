﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class TokenRangeTest
    {
        [TestMethod]
        public void ZeroZero()
        {
            TokenRange tokenRange = new TokenRange(0, 0);

            Assert.AreEqual((uint)0, tokenRange.Start);
            Assert.AreEqual((uint)0, tokenRange.End);
        }

        [TestMethod]
        public void ZeroOne()
        {
            TokenRange tokenRange = new TokenRange(0, 1);

            Assert.AreEqual((uint)0, tokenRange.Start);
            Assert.AreEqual((uint)1, tokenRange.End);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void BadArguments()
        {
            TokenRange tokenRange = new TokenRange(1, 0);
        }

        [TestMethod]
        public void HashCode()
        {
            TokenRange tokenRange = new TokenRange(0, 1);
            TokenRange tokenRange2 = new TokenRange(0, 1);

            Assert.AreEqual(tokenRange.GetHashCode(), tokenRange2.GetHashCode());
        }

        [TestMethod]
        public void HashCode2()
        {
            TokenRange tokenRange = new TokenRange(0, 0);
            TokenRange tokenRange2 = new TokenRange(0, 1);

            Assert.AreNotEqual(tokenRange.GetHashCode(), tokenRange2.GetHashCode());
        }

        [TestMethod]
        public void EqualsTest()
        {
            TokenRange tokenRange = new TokenRange(0, 1);
            TokenRange tokenRange2 = new TokenRange(0, 1);

            Assert.IsTrue(tokenRange.Equals(tokenRange2));
        }

        [TestMethod]
        public void EqualsTest2()
        {
            TokenRange tokenRange = new TokenRange(0, 0);
            TokenRange tokenRange2 = new TokenRange(1, 1);

            Assert.IsFalse(tokenRange.Equals(tokenRange2));
        }
    }
}
