﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class EndOfLineTokenTest
    {
        [TestMethod]
        public void ValuesTest()
        {
            EndOfLineToken token = new EndOfLineToken(new TokenRange(1,2), "\r");

            Assert.AreEqual(TokenType.EndOfLine, token.Type);
            Assert.AreEqual("\r", token.Text);
            Assert.AreEqual(new TokenRange(1,2), token.TokenRange);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RangeNullTest()
        {
            EndOfLineToken token = new EndOfLineToken(null, "\r");           
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TextNullTest()
        {
            EndOfLineToken token = new EndOfLineToken(new TokenRange(0,0), null);
        }
    }
}
