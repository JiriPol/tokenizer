﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class WhitespaceTokenTest
    {
        [TestMethod]
        public void ValuesTest()
        {
            WhitespaceToken token = new WhitespaceToken(new TokenRange(0,0), " ");

            Assert.IsNotNull(token);
            Assert.AreEqual(new TokenRange(0,0), token.TokenRange);
            Assert.AreEqual(" ", token.Text);
            Assert.AreEqual(TokenType.Whitespace, token.Type);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RangeNullTest()
        {
            WhitespaceToken token = new WhitespaceToken(null, "\r");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TextNullTest()
        {
            WhitespaceToken token = new WhitespaceToken(new TokenRange(0, 0), null);
        }
    }
}
