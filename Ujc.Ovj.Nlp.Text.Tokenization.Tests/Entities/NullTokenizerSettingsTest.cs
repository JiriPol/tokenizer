﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class NullTokenizerSettingsTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            NullTokenizerSettings settings = new NullTokenizerSettings();

            Assert.IsNotNull(settings);
            Assert.IsNotNull(settings.PunctuationList);
            Assert.IsNotNull(settings.AbbrevitationList);
            Assert.IsNotNull(settings.NewLineList);
            Assert.IsNotNull(settings.WhitespaceList);
            Assert.IsNotNull(settings.WordPartList);


            Assert.IsTrue(settings.PunctuationList.Count == 0);
            Assert.IsTrue(settings.AbbrevitationList.Count == 0);
            Assert.IsTrue(settings.NewLineList.Count == 0);
            Assert.IsTrue(settings.WhitespaceList.Count == 0);
            Assert.IsTrue(settings.WordPartList.Count == 0);
        }
    }
}
