﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Entities;
using Ujc.Ovj.Nlp.Text.Tokenization.Types;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests.Entities
{
    [TestClass]
    public class EndOfStreamTokenTest
    {
        [TestMethod]
        public void ValuesTest()
        {
            EndOfStreamToken token = new EndOfStreamToken(0);

            Assert.IsNotNull(token);
            Assert.AreEqual("", token.Text);
            Assert.AreEqual(TokenType.EndOfStream, token.Type);
            Assert.AreEqual(new TokenRange(0,0), token.TokenRange);
        }
    }
}
