﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ujc.Ovj.Nlp.Text.Tokenization.Interfaces;

namespace Ujc.Ovj.Nlp.Text.Tokenization.Tests
{
    [TestClass]
    public class SimpleWordTokenizerSettingsReaderXmlTest
    {
        [TestMethod]
        public void Constructor()
        {
            SimpleWordTokenizerSettingsReaderXml readerXml = new SimpleWordTokenizerSettingsReaderXml(Stream.Null);

            Assert.IsNotNull(readerXml);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorNullParam()
        {
            new SimpleWordTokenizerSettingsReaderXml(null);
        }

        [TestMethod]
        public void ReadWhitespaceHexChars()
        {
            SimpleWordTokenizerSettingsReaderXml readerXml = new SimpleWordTokenizerSettingsReaderXml(GenerateStreamFromString(XML_WHITESPACE_HEX_CHARS));
            ITokenizerSettings tokenizerSettings = readerXml.Read();

            char[] array = tokenizerSettings.WhitespaceList.ToArray();

            Assert.AreEqual(3, array.Length);
            Assert.AreEqual('\r', array[0]);
            Assert.AreEqual(' ', array[1]);
            Assert.AreEqual('\n', array[2]);
        }

        private const string XML_WHITESPACE_HEX_CHARS = @"<?xml version=""1.0"" encoding=""utf-8""?>" + "\r\n" +
                                                        @"<settings xmlns=""http://vokabular.ujc.cas.cz/schemas/diachronic-nlp/setting"">" + "\r\n" +
                                                        @"<setting type=""SimpleWordTokenizer"">" + "\r\n" +
                                                        @"<whiteSpaces type=""chars"" format=""hex"">" + "\r\n" +
                                                        @"<item hint=""SPACE (U+0020)"">0x0D</item>" + "\r\n" +
                                                        @"<item hint=""LINE FEED (LF) (U+000A)"">0x20</item>" + "\r\n" +
                                                        @"<item hint=""CARRIAGE RETURN (CR) (U+000D)"">0x0A</item>" + "\r\n" +
                                                        @"</whiteSpaces>" +
                                                        "</setting>\r\n" +
                                                        @"</settings>";

        [TestMethod]
        public void ReadWhitespaceChars()
        {
            SimpleWordTokenizerSettingsReaderXml readerXml = new SimpleWordTokenizerSettingsReaderXml(GenerateStreamFromString(XML_WHITESPACE_CHARS));
            ITokenizerSettings tokenizerSettings = readerXml.Read();

            char[] array = tokenizerSettings.WhitespaceList.ToArray();

            Assert.AreEqual(3, array.Length);
            Assert.AreEqual('a', array[0]);
            Assert.AreEqual('q', array[1]);
            Assert.AreEqual('p', array[2]);
        }

        private const string XML_WHITESPACE_CHARS = @"<?xml version=""1.0"" encoding=""utf-8""?>" + "\r\n" +
                                   @"<settings xmlns=""http://vokabular.ujc.cas.cz/schemas/diachronic-nlp/setting"">" + "\r\n" + 
                                       @"<setting type=""SimpleWordTokenizer"">" + "\r\n" + 
                                           @"<whiteSpaces type=""chars"">" + "\r\n" + 
                                               @"<item>a</item>" + "\r\n" + 
                                               @"<item>q</item>" + "\r\n" + 
                                               @"<item>p</item>" + "\r\n" + 
                                           @"</whiteSpaces>" +
                                       "</setting>\r\n" +
                                   @"</settings>";

        [TestMethod]
        public void ReadWhitespaceClassicAndHexChars()
        {
            SimpleWordTokenizerSettingsReaderXml readerXml = new SimpleWordTokenizerSettingsReaderXml(GenerateStreamFromString(XML_WHITESPACE_CLASSIC_HEX_CHARS));
            ITokenizerSettings tokenizerSettings = readerXml.Read();

            char[] array = tokenizerSettings.WhitespaceList.ToArray();

            Assert.AreEqual(6, array.Length);
            Assert.AreEqual('a', array[0]);
            Assert.AreEqual('q', array[1]);
            Assert.AreEqual('p', array[2]);
            Assert.AreEqual('\r', array[3]);
            Assert.AreEqual(' ', array[4]);
            Assert.AreEqual('\n', array[5]);
        }

        private const string XML_WHITESPACE_CLASSIC_HEX_CHARS = @"<?xml version=""1.0"" encoding=""utf-8""?>" + "\r\n" +
                                                    @"<settings xmlns=""http://vokabular.ujc.cas.cz/schemas/diachronic-nlp/setting"">" + "\r\n" +
                                                    @"<setting type=""SimpleWordTokenizer"">" + "\r\n" +
                                                    @"<whiteSpaces type=""chars"">" + "\r\n" +
                                                    @"<item>a</item>" + "\r\n" +
                                                    @"<item>q</item>" + "\r\n" +
                                                    @"<item>p</item>" + "\r\n" +
                                                    @"</whiteSpaces>" +
                                                    @"<whiteSpaces type=""chars"" format=""hex"">" + "\r\n" +
                                                    @"<item hint=""SPACE (U+0020)"">0x0D</item>" + "\r\n" +
                                                    @"<item hint=""LINE FEED (LF) (U+000A)"">0x20</item>" + "\r\n" +
                                                    @"<item hint=""CARRIAGE RETURN (CR) (U+000D)"">0x0A</item>" + "\r\n" +
                                                    @"</whiteSpaces>" +
                                                    "</setting>\r\n" +
                                                    @"</settings>";

        [TestMethod]
        public void ReadPunctuationHexChars()
        {
            SimpleWordTokenizerSettingsReaderXml readerXml = new SimpleWordTokenizerSettingsReaderXml(GenerateStreamFromString(XML_PUNCTUATION_HEX_CHARS));
            ITokenizerSettings tokenizerSettings = readerXml.Read();

            char[] array = tokenizerSettings.PunctuationList.ToArray();

            Assert.AreEqual(3, array.Length);
            Assert.AreEqual('[', array[0]);
            Assert.AreEqual(']', array[1]);
            Assert.AreEqual('\'', array[2]);
        }

        private const string XML_PUNCTUATION_HEX_CHARS = @"<?xml version=""1.0"" encoding=""utf-8""?>" + "\r\n" +
                                                        @"<settings xmlns=""http://vokabular.ujc.cas.cz/schemas/diachronic-nlp/setting"">" + "\r\n" +
                                                        @"<setting type=""SimpleWordTokenizer"">" + "\r\n" +
                                                        @"<punctaionCharacters type=""chars"" format=""hex"">" + "\r\n" +
                                                        @"<item hint=""LEFT SQUARE BRACKET (U+005B)"">0x5B</item>" + "\r\n" +
                                                        @"<item hint=""RIGHT SQUARE BRACKET (U+005D)"">0x5D</item>" + "\r\n" +
                                                        @"<item hint=""APOSTROPHE (U+0027)"">0x27</item>" + "\r\n" +
                                                        @"</punctaionCharacters>" +
                                                        "</setting>\r\n" +
                                                        @"</settings>";

        [TestMethod]
        public void ReadPunctuationChars()
        {
            SimpleWordTokenizerSettingsReaderXml readerXml = new SimpleWordTokenizerSettingsReaderXml(GenerateStreamFromString(XML_PUNCTUATION_CHARS));
            ITokenizerSettings tokenizerSettings = readerXml.Read();

            char[] array = tokenizerSettings.PunctuationList.ToArray();

            Assert.AreEqual(3, array.Length);
            Assert.AreEqual('[', array[0]);
            Assert.AreEqual(']', array[1]);
            Assert.AreEqual('\'', array[2]);
        }

        private const string XML_PUNCTUATION_CHARS = @"<?xml version=""1.0"" encoding=""utf-8""?>" + "\r\n" +
                                                         @"<settings xmlns=""http://vokabular.ujc.cas.cz/schemas/diachronic-nlp/setting"">" + "\r\n" +
                                                         @"<setting type=""SimpleWordTokenizer"">" + "\r\n" +
                                                         @"<punctaionCharacters type=""chars"">" + "\r\n" +
                                                         @"<item>[</item>" + "\r\n" +
                                                         @"<item>]</item>" + "\r\n" +
                                                         @"<item>'</item>" + "\r\n" +
                                                         @"</punctaionCharacters>" +
                                                         "</setting>\r\n" +
                                                         @"</settings>";

        public static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }
    }
}